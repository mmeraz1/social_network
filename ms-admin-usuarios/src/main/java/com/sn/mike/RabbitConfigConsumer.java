package com.sn.mike;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitConfigConsumer {

	public final static String QUEUE_USUARIOS = "queue-usuarios";
	public final static String QUEUE_MENSAJES= "queue-usuarios";
	public final static String QUEUE_PUBLICACIONES = "queue-usuarios";
	public final static String QUEUE_CHATS = "queue-usuarios";
	public final static String QUEUE_COMENTARIOS = "queue-administracion-confirmacion";

	@Bean
	Queue queue() {
		return new Queue(QUEUE_USUARIOS);
	}

	@Bean
	Queue queue2() {
		return new Queue(QUEUE_MENSAJES);
	}
	
	@Bean
	Queue queue3() {
		return new Queue(QUEUE_PUBLICACIONES);
	}

	@Bean
	Queue queue4() {
		return new Queue(QUEUE_CHATS);
	}

	@Bean
	Queue queue5() {
		return new Queue(QUEUE_COMENTARIOS);
	}


	@Bean
	TopicExchange exchange() {
		return new TopicExchange("exchange-principal");
	}

	@Bean
	Binding binding(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(QUEUE_USUARIOS);
	}

	@Bean
	Binding binding2(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(QUEUE_MENSAJES);
	}
	@Bean
	Binding binding3(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(QUEUE_PUBLICACIONES);
	}

	@Bean
	Binding binding4(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(QUEUE_CHATS);
	}
	@Bean
	Binding binding5(Queue queue, TopicExchange exchange) {
		return BindingBuilder.bind(queue).to(exchange).with(QUEUE_COMENTARIOS);
	}

}
