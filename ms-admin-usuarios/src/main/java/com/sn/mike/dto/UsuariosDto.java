package com.sn.mike.dto;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuariosDto {

	private String id;
	private String nombre;
	private String apellido;
	private Date fecha;
	private Time hora;
	private String correo;
    private List<MensajesDto> mensajesList;
    private List<PublicacionesDto> publicacionesList;
    private List<ChatDto> chatsList;
    
    
    
}