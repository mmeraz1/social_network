package com.sn.mike.dto;

import java.sql.Time;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublicacionesDto {

	private String id;
	private String imagen;
	private String contenido;
	private Date fecha;
	private Time hora;
	private UsuariosDto idUsuario;

	
}
