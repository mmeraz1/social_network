package com.sn.mike.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@JsonInclude(Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublicacionMensajeDto {

	private String id;
    private MensajesDto mensaje;
	private PublicacionesDto publicacion;
	
	
}
