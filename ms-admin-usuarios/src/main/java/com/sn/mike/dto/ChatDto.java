package com.sn.mike.dto;

import java.util.Date;
import java.sql.Time;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatDto {

	private String id;
	private String nombre;
	private Date fecha;
	private Time hora;
	private UsuariosDto idUsuario;
	
}
