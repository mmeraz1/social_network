package com.sn.mike.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sn.mike.dto.ChatDto;
import com.sn.mike.dto.MensajesDto;
import com.sn.mike.dto.PublicacionesDto;
import com.sn.mike.dto.RespuestaDto;
import com.sn.mike.dto.UsuariosDto;
import com.sn.mike.service.AdminUsuariosService;

@RestController
@RequestMapping("/admUsuario")
public class AdminUsuariosController {

	@Autowired
	private AdminUsuariosService adminUsuarioService;
	
	@GetMapping("/{idUsuario}")
	public UsuariosDto usuarioPorId(@PathVariable("idUsuario") String idUsuario) {
		return this.adminUsuarioService.usuarioPorId(idUsuario);
	}
	
	@GetMapping("/publicaciones/{idUsuario}")
	public List<PublicacionesDto> publicacionesPorUsuario(@PathVariable("idUsuario") String idUsuario) {
		return this.adminUsuarioService.publicacionesUsuario(idUsuario);
	}
	
	@GetMapping("/mensajes/{idUsuario}")
	public List<MensajesDto> mensajesPorUsuario(@PathVariable("idUsuario") String idUsuario) {
		return this.adminUsuarioService.mensajesUsuario(idUsuario);
	}
	
	@GetMapping("/chats/{idUsuario}")
	public RespuestaDto<List<ChatDto>> chatsPorUsuario(@PathVariable("idUsuario") String idUsuario) {
		return this.adminUsuarioService.chatsUsuario(idUsuario);
	}
	
	@PostMapping()
	public String crearUsuario(@RequestBody UsuariosDto idUsuario) {
		return this.adminUsuarioService.crearUsuario(idUsuario);
	}
	
	
	@PutMapping()
	public String editarUsuario(@RequestBody UsuariosDto idUsuario) {
		return this.adminUsuarioService.editarUsuario(idUsuario);
	}
}