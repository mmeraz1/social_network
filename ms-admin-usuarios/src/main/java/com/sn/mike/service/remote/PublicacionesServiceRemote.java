package com.sn.mike.service.remote;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.sn.mike.dto.PublicacionesDto;

@FeignClient(name="entity-service-publicaciones",
fallback=PublicacionesServiceFallback.class)
public interface PublicacionesServiceRemote {

	@GetMapping("/publicaciones/porUsuario/{idUsuario}")
	List<PublicacionesDto> publicacionesPorUsuario(@PathVariable("idUsuario") String idUsuario);
}
