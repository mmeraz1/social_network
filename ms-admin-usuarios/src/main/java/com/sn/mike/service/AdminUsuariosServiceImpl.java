package com.sn.mike.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.sn.mike.dto.ChatDto;
import com.sn.mike.dto.MensajesDto;
import com.sn.mike.dto.PublicacionesDto;
import com.sn.mike.dto.RespuestaDto;
import com.sn.mike.dto.UsuariosDto;
import com.sn.mike.service.remote.ChatServiceRemote;
import com.sn.mike.service.remote.MensajesServiceRemote;
import com.sn.mike.service.remote.PublicacionesServiceRemote;
import com.sn.mike.service.remote.UsuariosServiceRemote;
import com.sn.mike.service.remote.async.AdminUsuariosConsumer;

@Service
public class AdminUsuariosServiceImpl implements AdminUsuariosService {

	public static final Logger logger = LoggerFactory.getLogger(AdminUsuariosServiceImpl.class);
	
	
	@Autowired
	private ChatServiceRemote chatServiceRemote;
	
	@Autowired
	private MensajesServiceRemote mensajesServiceRemote;
	
	@Autowired
	private PublicacionesServiceRemote publicacionesServiceRemote;
	
	@Autowired
	private UsuariosServiceRemote usuariosServiceRemote;
	
	@Override
	public List<MensajesDto> mensajesUsuario(String idUsuario) {
		try {
			
			return mensajesServiceRemote.mensajesPorUsuario(idUsuario);
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<MensajesDto>();
		}
	}

	@Override
	public List<PublicacionesDto> publicacionesUsuario(String idUsuario) {
//		try {
			
			return publicacionesServiceRemote.publicacionesPorUsuario(idUsuario);
//		}catch (Exception e) {
//			e.printStackTrace();
//			return new ArrayList<PublicacionesDto>();
//		}
	}

	@Override
	public RespuestaDto<List<ChatDto>> chatsUsuario(String idUsuario) {
		try {
		
			List<ChatDto> chats = chatServiceRemote.chatsPorUsuario(idUsuario);
			
			return new RespuestaDto<List<ChatDto>>(chats, true, "Se obtuvieron los mensajes");
			
		} catch(Exception e) {
			logger.info(e.getMessage());
			e.printStackTrace();
			return  new RespuestaDto(null, false, "ocurrio un error al consultar mensajes");
		}
		
		
	}
	
public List<ChatDto> chatsUsuarioRB(List<ChatDto> chats) {
		return chats;
	}
	

	@Override
	public UsuariosDto usuarioPorId(String idUsuario) {
		return usuariosServiceRemote.usuarioPorid(idUsuario);
	}

	@Override
	public String crearUsuario(UsuariosDto dto) {
		try{
			usuariosServiceRemote.insert(dto);
			return "El usuario se creo satisfactoriamente";			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("error al agregar usuario ");
		return "Ocurrio un error al crear el usuario, intente mas tarde";
		}
	}

	@Override
	public String editarUsuario(UsuariosDto dto) {
		try{
			usuariosServiceRemote.insert(dto);
			return "El usuario se edito satisfactoriamente";			
		} catch (Exception e) {
			e.printStackTrace();
			logger.info("error al editar usuario ");
		return "Ocurrio un error al editar el usuario, intente mas tarde";
		}
	}
	
}
