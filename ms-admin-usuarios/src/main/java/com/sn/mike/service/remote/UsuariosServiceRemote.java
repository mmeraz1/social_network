package com.sn.mike.service.remote;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.sn.mike.dto.UsuariosDto;

@FeignClient("entity-service-usuarios")
public interface UsuariosServiceRemote {

	@PostMapping("/usuarios")
	Boolean insert(@RequestBody UsuariosDto dto);
	
	
	@GetMapping("/usuarios")
	List<UsuariosDto> findAll();
	
	@GetMapping("/usuarios/{idUsuario}")
	UsuariosDto usuarioPorid(@PathVariable("idUsuario") String idUsuario); 
}
