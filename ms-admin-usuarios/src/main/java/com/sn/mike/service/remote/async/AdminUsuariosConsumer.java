package com.sn.mike.service.remote.async;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import com.google.gson.Gson;
import com.sn.mike.RabbitConfigConsumer;
import com.sn.mike.dto.MensajesDto;
import com.sn.mike.service.AdminUsuariosServiceImpl;

@Component
public class AdminUsuariosConsumer {

	public static final Logger logger = LoggerFactory.getLogger(AdminUsuariosConsumer.class);
	private Gson json = new Gson();
	private  AdminUsuariosServiceImpl adminUsuariosServiceImpl;

	public AdminUsuariosConsumer(AdminUsuariosServiceImpl adminUsuariosServiceImpl) {
		this.adminUsuariosServiceImpl = adminUsuariosServiceImpl;
	}

//	@RabbitListener(queues = RabbitConfigConsumer.QUEUE_CHATS)
//	public void listenMensajes(Message message) {
//		String body = new String(message.getBody());
//		logger.info("Event confirmacion del adm de autorizaciones" + body);
//
//		AprobacionDto aprobacionDto = new AprobacionDto();
//
//		ConfirmacionDto confirmacionDto = json.fromJson(body, ConfirmacionDto.class); 
//		
//		String transforma = confirmacionDto.getDatosValidacion().replace("\"{", "{").replace("\\", "").replace("}\"", "}");
//		
//		CreditosDto creditosDto = json.fromJson(transforma, CreditosDto.class);
//
//		aprobacionDto.setIdCredito(creditosDto.getIdCredito());
//		aprobacionDto.setEstatus(confirmacionDto.getAutorizacion());
//
//		aprobacionesService.guardar(aprobacionDto);
//		
//		logger.info("creditos autorizaciones ok");
//	}
//
	
	@RabbitListener(queues = RabbitConfigConsumer.QUEUE_CHATS)
	public void listenMensajes(Message message) {
		String body = new String(message.getBody());
		logger.info("Evento de la obtencion de los mensajes por usuario" + body);

		List<MensajesDto> mensajesDto = (List<MensajesDto>) json.fromJson(body, MensajesDto.class);


		
		logger.info("se obtuvieron los mensajes correctamente");
	}
	
	
	
//	@RabbitListener(queues = RabbitConfigConsumer.QUEUE_MENSAJES)
//	public void escucharMensajes(Message message) throws JsonParseException, JsonMappingException, IOException {
//		String body = new String(message.getBody());
//		logger.info("Event autorizaciones" + body);
//        ObjectMapper mapper = new ObjectMapper();
//		
//			List<MensajesDto> entities =  (List<MensajesDto>) mapper.readValue(body,MensajesDto.class);
//
//
//		administracionServiceImpl.mensajesUsuario(entities);
//	}
}

