package com.sn.mike.service.remote;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.sn.mike.dto.ChatDto;

@FeignClient("entity-service-chat")
public interface ChatServiceRemote {

	@GetMapping("/chat/porUsuario/{idUsuario}")
	List<ChatDto> chatsPorUsuario(@PathVariable("idUsuario") String idUsuario);
}
