package com.sn.mike.service.remote;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.springframework.stereotype.Component;

import com.sn.mike.dto.PublicacionesDto;

@Component
public class PublicacionesServiceFallback implements PublicacionesServiceRemote {
    


	@Override
	public List<PublicacionesDto> publicacionesPorUsuario(String idUsuario) {
		System.out.println("Paso por el fallback");
		return new ArrayList<PublicacionesDto>();
	}
}
