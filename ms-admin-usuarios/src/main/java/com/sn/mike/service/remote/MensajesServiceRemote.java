package com.sn.mike.service.remote;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.sn.mike.dto.MensajesDto;

@FeignClient("entity-service-mensajes")
public interface MensajesServiceRemote {

	@GetMapping("/mensajes/porUsuario/{idUsuario}")
	List<MensajesDto> mensajesPorUsuario(@PathVariable("idUsuario") String idUsuario);
	
}

