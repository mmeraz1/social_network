package com.sn.mike.service;

import java.util.List;

import com.sn.mike.dto.ChatDto;
import com.sn.mike.dto.RespuestaDto;
import com.sn.mike.dto.MensajesDto;
import com.sn.mike.dto.PublicacionesDto;
import com.sn.mike.dto.UsuariosDto;

public interface AdminUsuariosService {

	
	List<MensajesDto> mensajesUsuario(String idUsuario);
	List<PublicacionesDto> publicacionesUsuario(String idUsuario);
	RespuestaDto<List<ChatDto>> chatsUsuario(String idUsuario);
	UsuariosDto usuarioPorId(String idUsuario);
	String crearUsuario(UsuariosDto dto);
	String editarUsuario(UsuariosDto dto);
}
