# mvn compile jib:dockerBuild
mvn install:install-file -Dfile=${PWD}/../utility-service/target/utility-service-0.0.1-SNAPSHOT.jar -DgroupId=com.sn.mike -DartifactId=utility-service -Dversion=0.0.1-SNAPSHOT -Dpackaging=jar -DgeneratePom=true
mvn install -Dmaven.test.skip=true
docker build -t mmeraz2/ms-admin-usuarios:0.0.1-SNAPSHOT .