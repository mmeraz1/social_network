package com.sn.mike.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sn.mike.dao.MensajesDao;
import com.sn.mike.dao.PublicacionMensajeDao;
import com.sn.mike.dto.MensajesDto;
import com.sn.mike.entities.Mensajes;

@Service
public class MensajesServiceImpl implements MensajesService {

	
	@Autowired
	private MensajesDao mensajeDao;
	
	@Override
	public List<MensajesDto> finByAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<MensajesDto> findByConversation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MensajesDto finById(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean update(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MensajesDto insertarEditarMensaje(MensajesDto dto) {
		try {
			
			Mensajes mensajeInsertado = mensajeDao.save(dto.to());
			return mensajeInsertado.to();
			
		} catch (Exception e) {
			e.printStackTrace();
			return new MensajesDto();
		}
	}

	@Override
	public List<MensajesDto> mensajesPorUsuario(String idUsuario) {
		try {
			System.out.println("id del usuario => " +  idUsuario);
			List<Mensajes> list = mensajeDao.findByIdUsuarioId(idUsuario);
			return list.stream().map(m -> m.to()).collect(Collectors.toList());
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<MensajesDto>();
		}
	}

}
