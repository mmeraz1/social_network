package com.sn.mike.service;

import java.util.List;

import com.sn.mike.dto.MensajesDto;
import com.sn.mike.dto.PublicacionMensajeDto;

public interface MensajesService {

	List<MensajesDto> finByAll();
	List<MensajesDto> findByConversation();
	MensajesDto finById(String id);
	Boolean update(String id);
	Boolean delete(String id);
	MensajesDto insertarEditarMensaje(MensajesDto dto);
	List<MensajesDto> mensajesPorUsuario(String idUsuario);
	
}
