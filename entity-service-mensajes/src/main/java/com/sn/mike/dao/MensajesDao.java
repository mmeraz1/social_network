package com.sn.mike.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sn.mike.entities.Mensajes;

@Repository
public interface MensajesDao extends JpaRepository<Mensajes, String> {

	
	List<Mensajes> findByIdUsuarioId(String idUsuario);
}
