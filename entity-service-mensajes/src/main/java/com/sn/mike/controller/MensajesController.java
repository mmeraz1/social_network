package com.sn.mike.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sn.mike.dto.MensajesDto;
import com.sn.mike.service.MensajesService;

@RestController
@RequestMapping("/mensajes")
public class MensajesController {

	@Autowired
	private MensajesService mensajesService;
	
	@PostMapping
	public MensajesDto insertarEditarMensajes(@RequestBody MensajesDto dto) {
		return this.mensajesService.insertarEditarMensaje(dto);
	}
	
	
	@GetMapping("/porUsuario/{idUsuario}")
	public List<MensajesDto> mensajesPorUsuario(@PathVariable("idUsuario") String idUsuario){
		return this.mensajesService.mensajesPorUsuario(idUsuario);
	}
}
