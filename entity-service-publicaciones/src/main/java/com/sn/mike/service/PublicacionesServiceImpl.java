package com.sn.mike.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sn.mike.dao.PublicacionesDao;
import com.sn.mike.dto.PublicacionesDto;
import com.sn.mike.entities.Publicaciones;

@Service
public class PublicacionesServiceImpl implements PublicacionesService {

	@Autowired
	private PublicacionesDao publicacionDao;
	
	@Override
	public Boolean insertUpdate(PublicacionesDto dto) {
		Publicaciones publicacionNueva = publicacionDao.save(dto.to());
		return !publicacionNueva.equals(null);
	}

	@Override
	public List<PublicacionesDto> publicacionesPorUsuario(String idUsuario) {
//		try {
			List<Publicaciones> list = publicacionDao.findByIdUsuarioId(idUsuario);
			return list.stream().map(p -> p.to()).collect(Collectors.toList());
			
//		} catch (Exception e) {
//			e.printStackTrace();
//			return new ArrayList<PublicacionesDto>();
//		}
	}

}
