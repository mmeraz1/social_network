package com.sn.mike.service;

import java.util.List;

import com.sn.mike.dto.PublicacionesDto;

public interface PublicacionesService {

	Boolean insertUpdate(PublicacionesDto dto);
	
	List<PublicacionesDto> publicacionesPorUsuario(String idUsuario);
	
	
	
}
