package com.sn.mike.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sn.mike.dto.PublicacionesDto;
import com.sn.mike.service.PublicacionesService;

@RestController
@RequestMapping("/publicaciones")
public class PublicacionesController {

	@Autowired
	private PublicacionesService publicacionService;
	
	@Autowired
	private CircuitBreakerFactory cbFactory;
	
	
	@PostMapping
	public Boolean insert(@RequestBody PublicacionesDto dto) {
		return this.publicacionService.insertUpdate(dto);
	}
	
	@GetMapping("/porUsuario/{idUsuario}")
	public List<PublicacionesDto> publicacionesPorUsuario(@PathVariable("idUsuario") String idusuario) throws InterruptedException{
		return cbFactory.create("publicaciones")
				.run(() -> publicacionService.publicacionesPorUsuario(idusuario), e -> {
				System.out.println("Fallo el servicio");
				return new ArrayList<PublicacionesDto>();
				} );
				}
	}