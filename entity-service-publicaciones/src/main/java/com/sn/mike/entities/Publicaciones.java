package com.sn.mike.entities;

import java.util.Date;
import java.io.Serializable;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.sn.mike.dto.PublicacionesDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name="publicaciones")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Publicaciones implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(updatable = false)
	private String id;
	private String imagen;
	private String contenido;
	private Date fecha;
	private Time hora;
    @ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuarios idUsuario;
    
	public Publicaciones(String id) {
		this.id = id;
	}
	
	
	public PublicacionesDto to() {
		PublicacionesDto dto = new PublicacionesDto();
		dto.setId(getId());
		dto.setContenido(getContenido());
		dto.setFecha(getFecha());
		dto.setHora(getHora());
		dto.setImagen(getImagen());
		dto.setIdUsuario(getIdUsuario().to());
		return dto;
	}
	
    
    
    
}
