package com.sn.mike.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sn.mike.entities.Publicaciones;

@Repository
public interface PublicacionesDao extends JpaRepository<Publicaciones , String> {

	
	List<Publicaciones> findByIdUsuarioId(String idUsuario);
}
