package com.sn.mike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class EntityServiceUsuariosApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntityServiceUsuariosApplication.class, args);
	}

}
