package com.sn.mike.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sn.mike.entities.Usuarios;

@Repository
public interface UsuariosDao extends JpaRepository<Usuarios, String> {

}
