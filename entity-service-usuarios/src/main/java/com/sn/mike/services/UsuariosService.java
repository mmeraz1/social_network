package com.sn.mike.services;

import java.util.List;

import com.sn.mike.dto.UsuariosDto;
import com.sn.mike.entities.Usuarios;

public interface UsuariosService {

	List<UsuariosDto> findAll();
	List<UsuariosDto> findByConversation();
	Usuarios findById();
	Boolean insert(UsuariosDto dto);
	Boolean update(UsuariosDto dto);
	Boolean delete(String id);
	UsuariosDto usuarioPorId(String id);
}
