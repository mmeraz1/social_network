package com.sn.mike.services;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sn.mike.dao.UsuariosDao;
import com.sn.mike.dto.UsuariosDto;
import com.sn.mike.entities.Usuarios;

@Service
public class UsuariosServiceImpl implements UsuariosService {

	@Autowired
	private UsuariosDao usuarioDao;
	
	
	
	@Override
	public List<UsuariosDto> findAll() {
		try {
			
			return usuarioDao.findAll().stream().map(u -> u.to()).collect(Collectors.toList());
		} catch (Exception e) {
			
			return null;
		}
//		List<UsuariosDto> list = usuarioDao.findAll().stream().map(u -> u.to()).toList();
	}

	@Override
	public List<UsuariosDto> findByConversation() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Usuarios findById() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean insert(UsuariosDto dto) {
		Usuarios usuarioGuardado = usuarioDao.save(dto.to());
			
		return !usuarioGuardado.equals(null);
	}

	@Override
	public Boolean update(UsuariosDto dto) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Boolean delete(String id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UsuariosDto usuarioPorId(String id) {
		Optional<Usuarios> usuario = usuarioDao.findById(id);
		return usuario.isPresent() ? usuario.get().to() : new UsuariosDto();
	}


	
}
