package com.sn.mike.dto;

import java.util.Date;
import java.sql.Time;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sn.mike.entities.Chat;
import com.sn.mike.entities.Usuarios;
import com.sn.mike.utils.Format;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChatDto {

	private String id;
	private String nombre;
	private Date fecha;
	private Time hora;
	private UsuariosDto idUsuario;
	
	
	public Chat to() {
		Chat entity = new Chat();
		Date date = getId() == null ? new Date() : getFecha();
		Time time = getId() == null ? Format.hour() : getHora();
		if( getId() != null) {
			entity.setId(getId());
		}
		entity.setFecha(date);
		entity.setHora(time);
		entity.setNombre(getNombre());
		entity.setIdUsuario(getIdUsuario().to());
		
		return entity;
	}
	
	public Chat toId() {
		Chat entity = new Chat();
		entity.setId(getId());
		return entity;
	}
}
