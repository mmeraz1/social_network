package com.sn.mike.dto;

import java.util.Date;
import java.sql.Time;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sn.mike.entities.Publicaciones;
import com.sn.mike.entities.Usuarios;
import com.sn.mike.utils.Format;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublicacionesDto {

	private String id;
	private String imagen;
	private String contenido;
	private Date fecha;
	private Time hora;
	private UsuariosDto idUsuario;

	
	
	public Publicaciones to() {
		Publicaciones entity = new Publicaciones();
		if(getId() != null) {
			entity.setId(getId());			
		}
		entity.setImagen(getImagen());
		entity.setContenido(getContenido());
		entity.setFecha(new Date());
		entity.setHora(Format.hour());
		entity.setIdUsuario(getIdUsuario().toId());
		return entity;
	}
	
	public Publicaciones toId() {
		return new Publicaciones(getId());
	}
}
