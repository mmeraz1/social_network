package com.sn.mike.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sn.mike.dto.UsuariosDto;
import com.sn.mike.services.UsuariosService;

@RestController
@RequestMapping("/usuarios")
public class UsuariosController {
	
	@Autowired
	private UsuariosService usuarioService;
	
	@PostMapping
	public Boolean insert(@RequestBody UsuariosDto dto) {
		System.out.println("Entro al insertar usuarios");
		return this.usuarioService.insert(dto);
	}
	
	@GetMapping
	public List<UsuariosDto> findAll(){
		return this.usuarioService.findAll();
	}
	
	
	@GetMapping("/{idUsuario}")
	public UsuariosDto usuarioPorid(@PathVariable("idUsuario") String idUsuario) {
		return this.usuarioService.usuarioPorId(idUsuario);
	}

}
