package com.sn.mike.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.sn.mike.dto.PublicacionMensajeDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="publicacion_mensaje")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PublicacionMensaje implements Serializable{

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(updatable = false)
	private String id;
	@JoinColumn(name = "id_mensaje", referencedColumnName = "id")
    @ManyToOne
    private Mensajes mensaje;
	@JoinColumn(name = "id_publicacion", referencedColumnName = "id")
    @ManyToOne
	private Publicaciones publicacion;

	
	public PublicacionMensajeDto to() {
		PublicacionMensajeDto dto = new PublicacionMensajeDto();
		dto.setId(getId());
		dto.setMensaje(getMensaje().to());
		dto.setPublicacion(getPublicacion().to());
		return dto;
	}
	
}
