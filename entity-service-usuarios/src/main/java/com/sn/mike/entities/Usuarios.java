package com.sn.mike.entities;

import java.io.Serializable;
import java.sql.Time;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.sn.mike.dto.UsuariosDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name="usuarios")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Usuarios implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(updatable = false)
	private String id;
	private String nombre;
	private String apellido;
	private Date fecha;
	private Time hora;
	private String correo;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id")
    private List<Mensajes> mensajesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id")
    private List<Publicaciones> publicacionesList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id")
    private List<Chat> chatsList;
    
    
    
    public Usuarios from(UsuariosDto dto) {
    	Usuarios entity = new Usuarios();
    	entity.setNombre(dto.getNombre());
    	entity.setApellido(dto.getApellido());
    	entity.setCorreo(dto.getCorreo());
    	entity.setFecha(dto.getFecha());
    	entity.setHora(dto.getHora());
    	return entity;
    }
    
    public UsuariosDto to() {
    	UsuariosDto dto = new UsuariosDto();
    	dto.setId(getId());
    	dto.setNombre(getNombre());
    	dto.setApellido(getApellido());
    	dto.setCorreo(getCorreo());
    	dto.setFecha(getFecha());
    	dto.setHora(getHora());
    	return dto;
    }

}
