package com.sn.mike.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RespuestaDto<T> {

	
	private T body;
	private Boolean estatus;
	private String codigo;
	List<T> cuerpo;
	public RespuestaDto(T body, Boolean estatus, String codigo) {
		this.body = body;
		this.estatus = estatus;
		this.codigo = codigo;
	}
	public RespuestaDto(List<T> cuerpo,Boolean estatus, String codigo) {
		super();
		this.estatus = estatus;
		this.codigo = codigo;
		this.cuerpo = cuerpo;
	}
	
	
	
	
}
