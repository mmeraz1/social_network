package com.sn.mike.dto;

import java.util.Date;
import java.sql.Time;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sn.mike.entities.Mensajes;
import com.sn.mike.entities.Usuarios;
import com.sn.mike.utils.Format;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MensajesDto {

	private String id;
	private String texto;
	private Date fecha;
	private Time hora;
	private UsuariosDto idUsuario;
	
	
	public Mensajes to() {
		Mensajes entity = new Mensajes();
		Date date = getId() == null ? new Date() : getFecha();
		Time hour = getId() == null ? Format.hour() : getHora();
		if(getId() != null) {
			entity.setId(getId());
		}
		entity.setTexto(getTexto());
		entity.setFecha(date);
		entity.setHora(hour);
		entity.setIdUsuario(getIdUsuario().toId());
		return entity;
	}
	
	public Mensajes toId() {
		return new Mensajes(getId());
	}

	public MensajesDto(String id) {
		this.id = id;
	}
	
	
}
