package com.sn.mike.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sn.mike.entities.PublicacionMensaje;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@JsonInclude(Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PublicacionMensajeDto {

	private String id;
    private MensajesDto mensaje;
	private PublicacionesDto publicacion;
	
	
	
	public PublicacionMensaje to() {
		PublicacionMensaje entity = new PublicacionMensaje();
		entity.setMensaje(getMensaje().toId());
		entity.setPublicacion(getPublicacion().toId());
		return entity;
	}
}
