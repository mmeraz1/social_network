
create table usuarios (
id varchar (255) primary key,
nombre varchar (50),
apellido varchar(50),
fecha date,
hora time
);


create table publicaciones (
id varchar (255) primary key,
imagen  varchar (10000),
contenido varchar,
fecha date,
hora time,
id_usuario varchar(255),
constraint fk_usuario_pub foreign key (id_usuario) references usuarios (id)
);


create table mensajes (
id varchar (255) primary key,
texto varchar,
fecha date,
hora time,
id_usuario varchar(255),
constraint  fk_usuario_men foreign key (id_usuario) references usuarios (id)
);


create table publicacion_mensaje (
id varchar (255) primary key,
id_publicacion varchar(255),
id_mensaje varchar(255),
constraint fk_publicacion_pub foreign key (id_publicacion) references publicaciones (id),
constraint fk_mensaje_pub foreign key (id_mensaje) references mensajes (id)
);



create table chat(
id varchar (255) primary key,
nombre varchar (500),
fecha date,
hora time,
id_usuario  varchar (255),
constraint fk_usuario_chat foreign key (id_usuario) references usuarios (id) 
);

create table mensaje_chat(
id varchar(255) primary key,
id_chat varchar(255),
id_mensaje varchar(255),
constraint fk_chat_men foreign key (id_chat) references chat (id),
constraint fk_men_men foreign key (id_mensaje) references mensajes (id)
);
