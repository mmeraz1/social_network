package com.sn.mike.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sn.mike.dto.ChatDto;
import com.sn.mike.service.ChatService;

@RestController
@RequestMapping("/chat")
public class ChatController {

	@Autowired
	private ChatService chatService;
	
	
	@PostMapping
	public Boolean crearEditarChat(@RequestBody ChatDto dto) {
		return this.chatService.crearEditarChat(dto);
	}
	
	@GetMapping("/porUsuario/{idUsuario}")
	public List<ChatDto> chatsPorUsuario(@PathVariable("idUsuario") String idUsuario){
		return this.chatService.chatsPorUsuario(idUsuario);
	}
	
}
