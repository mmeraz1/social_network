package com.sn.mike.service;

import java.util.List;

import com.sn.mike.dto.ChatDto;

public interface ChatService {

	Boolean crearEditarChat(ChatDto dto);
	List<ChatDto> chatsPorUsuario(String idUsuario);
}
