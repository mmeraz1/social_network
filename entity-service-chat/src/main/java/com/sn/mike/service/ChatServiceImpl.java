package com.sn.mike.service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.sn.mike.dao.ChatDao;
import com.sn.mike.dto.ChatDto;
import com.sn.mike.entities.Chat;
import com.sn.mike.service.remote.async.ChatProducer;

@Service
public class ChatServiceImpl implements ChatService{

	
	@Autowired
	private ChatDao chatDao;
	Gson json = new Gson();
	
	@Autowired
	private ChatProducer chatproducer;
	
	@Override
	public Boolean crearEditarChat(ChatDto dto) {
		Chat chat = chatDao.save(dto.to());
		
		return !chat.equals(null);
	}

	@Override
	public List<ChatDto> chatsPorUsuario(String idUsuario) {
		try {
			List<Chat> list = chatDao.findByIdUsuarioId(idUsuario);
			List<ChatDto> listDto = list.stream().map(c -> c.to()).collect(Collectors.toList());
			chatproducer.sendMessage(json.toJson(listDto));
			return listDto;
			
		} catch (Exception e) {
			e.printStackTrace();
			return new ArrayList<ChatDto>();
		}
	}
	
	
	
	
}
