package com.sn.mike;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class EntityServiceChatApplication {

	public static void main(String[] args) {
		SpringApplication.run(EntityServiceChatApplication.class, args);
	}

}
