package com.sn.mike.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.sn.mike.entities.Chat;
import com.sn.mike.entities.Usuarios;

@Repository
public interface ChatDao extends JpaRepository<Chat, String> {

	
	List<Chat> findByIdUsuarioId(String idUsuario);
	
	
	
}
