package com.sn.mike.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sn.mike.entities.Chat;
import com.sn.mike.entities.MensajeChat;
import com.sn.mike.entities.Mensajes;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MensajeChatDto {
	
	private String id;
	private ChatDto idChat;
	private MensajesDto idMensaje;
	
	
	public MensajeChat to() {
		MensajeChat entity = new MensajeChat();
		System.out.println("el id del chat => " + getIdChat().getId());
		System.out.println("el id del mensaje => " + getIdMensaje().getId());
		entity.setIdChat(getIdChat().toId());
		entity.setIdMensaje(getIdMensaje().toId());
		return entity;
	}

}
