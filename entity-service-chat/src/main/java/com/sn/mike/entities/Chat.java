package com.sn.mike.entities;

import java.util.Date;
import java.util.List;
import java.sql.Time;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.sn.mike.dto.ChatDto;
import com.sn.mike.utils.Format;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="chat")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Chat {

	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(updatable = false)
	private String id;
	private String nombre;
	private Date fecha;
	private Time hora;
    @ManyToOne
    @JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuarios idUsuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id")
    private List<MensajeChat> mensajesChatList;
    
    
	public ChatDto to() {
		ChatDto dto = new ChatDto();
		Date date = getId() == null ? new Date() : getFecha();
		Time time = getId() == null ? Format.hour() : getHora();
		if( getId() != null) {
			dto.setId(getId());
		}
		dto.setFecha(date);
		dto.setHora(time);
		dto.setNombre(getNombre());
		dto.setIdUsuario(getIdUsuario().to());
		
		return dto;
	}


	public Chat(String id) {
		this.id = id;
	}
	
	
    
}
