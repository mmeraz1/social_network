package com.sn.mike.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sn.mike.dto.PublicacionMensajeDto;
import com.sn.mike.service.PublicacionMensajeService;

@RestController
@RequestMapping("/publicacionMensaje")
public class PublicacionMensajeController {

	 @Autowired
	 private PublicacionMensajeService publicacionMensajeService;
	 
	 
	 @PostMapping
	 public String crearEditarMensajePublicacion(@RequestBody PublicacionMensajeDto dto) {
		 return this.publicacionMensajeService.crearEditarMensajePublicacion(dto);
	 }
	 
	 
	 @GetMapping("/{idPublicacion}")
	 public List<PublicacionMensajeDto> listIdPublicacion(@PathVariable("idPublicacion") String idPublicacion){
		 return this.publicacionMensajeService.mensajesPublicacion(idPublicacion);
	 }
	 
	  @DeleteMapping("/{idPublicacionMensaje}")
	  public Boolean eliminarMensajePublicacion(@PathVariable("idPublicacionMensaje") String idPublicacionMensaje) {
		  return this.publicacionMensajeService.borrarMensajePublicacion(idPublicacionMensaje);
	  }
	
}
