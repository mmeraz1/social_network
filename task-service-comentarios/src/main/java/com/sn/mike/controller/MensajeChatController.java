package com.sn.mike.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sn.mike.dto.MensajeChatDto;
import com.sn.mike.service.MensajeChatService;

@RestController
@RequestMapping("/mensajeChat")
public class MensajeChatController {

	@Autowired
	private MensajeChatService mensajeChatService;
	
	@PostMapping
	public String crearEditarMensajeChat(@RequestBody MensajeChatDto dto) {
		return this.mensajeChatService.crearEditarMensaje(dto);
	}
	
	
}
