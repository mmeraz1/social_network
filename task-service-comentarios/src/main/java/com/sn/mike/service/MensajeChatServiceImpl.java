package com.sn.mike.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sn.mike.dao.MensajeChatDao;
import com.sn.mike.dto.MensajeChatDto;
import com.sn.mike.dto.MensajesDto;
import com.sn.mike.entities.MensajeChat;
import com.sn.mike.entities.PublicacionMensaje;
import com.sn.mike.service.remote.MensajeServicesRemote;

@Service
public class MensajeChatServiceImpl implements MensajeChatService {

	@Autowired
	private MensajeChatDao mensajeChatDao;
	@Autowired
	private MensajeServicesRemote mensajeServiceRemote;
	@Override
	public String crearEditarMensaje(MensajeChatDto dto) {
		try {
			MensajesDto mensajeCreado = mensajeServiceRemote.insertarEditarMensajes(dto.getIdMensaje());
			if(!mensajeCreado.getId().isEmpty()) {
				dto.setIdMensaje(new MensajesDto(mensajeCreado.getId()));
				MensajeChat mensajePublicacion = mensajeChatDao.save(dto.to());
			return "Se creo exitosamente el comentario";
			}			
			
			return "Ocurrio un error al generar el comentario, favor de intentar nuevamente";
			
		} catch (Exception e) {
			e.printStackTrace();
			return "Ocurrio un error al agregar el mensaje, favor de intentar mas tarde";
		}
	}

	@Override
	public List<MensajeChatDto> mensajesPorChat(String idChat) {
		List<MensajeChat> list = mensajeChatDao.findByIdChatId(idChat);
		return list.stream().map(m -> m.to()).collect(Collectors.toList());
	}

}
