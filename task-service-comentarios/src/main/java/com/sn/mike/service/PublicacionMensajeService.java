package com.sn.mike.service;

import java.util.List;

import com.sn.mike.dto.PublicacionMensajeDto;

public interface PublicacionMensajeService {

	
	String crearEditarMensajePublicacion(PublicacionMensajeDto dto);
	List<PublicacionMensajeDto> mensajesPublicacion(String idPublicacion);
	Boolean borrarMensajePublicacion(String id);
}
