package com.sn.mike.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sn.mike.dao.PublicacionMensajeDao;
import com.sn.mike.dto.MensajesDto;
import com.sn.mike.dto.PublicacionMensajeDto;
import com.sn.mike.entities.PublicacionMensaje;
import com.sn.mike.service.remote.MensajeServicesRemote;

import lombok.extern.java.Log;

@Service
public class PublicacionMensajeServiceImpl implements PublicacionMensajeService {

	
	@Autowired
	private PublicacionMensajeDao publicacionMensajeDao;

	@Autowired
	private MensajeServicesRemote mensajeServiceRemote;
	
	@Override
	public String crearEditarMensajePublicacion(PublicacionMensajeDto dto) {
		MensajesDto mensajeCreado = mensajeServiceRemote.insertarEditarMensajes(dto.getMensaje());
		if(!mensajeCreado.getId().isEmpty()) {
			dto.setMensaje(new MensajesDto(mensajeCreado.getId()));
			PublicacionMensaje mensajePublicacion = publicacionMensajeDao.save(dto.to());
		return "Se creo exitosamente el comentario";
		}			
		
		return "Ocurrio un error al generar el comentario, favor de intentar nuevamente";
	}

	@Override
	public List<PublicacionMensajeDto> mensajesPublicacion(String idPublicacion) {
		List<PublicacionMensaje> list = publicacionMensajeDao.findByPublicacionId(idPublicacion);
		return list.stream().map(p -> p.to()).collect(Collectors.toList());
	}

	@Override
	public Boolean borrarMensajePublicacion(String id) {
		try {
			publicacionMensajeDao.deleteById(id);
			return true;
			
		} catch (Exception e) {
		return false;
		}
		
	}
}
