package com.sn.mike.service.remote;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.sn.mike.dto.MensajesDto;

@FeignClient("entity-service-mensajes")
public interface MensajeServicesRemote {

	
	@PostMapping("/mensajes")
	MensajesDto insertarEditarMensajes(@RequestBody MensajesDto dto); 
}
