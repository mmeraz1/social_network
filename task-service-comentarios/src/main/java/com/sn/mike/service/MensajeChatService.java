package com.sn.mike.service;

import java.util.List;

import com.sn.mike.dto.MensajeChatDto;

public interface MensajeChatService {

	String crearEditarMensaje(MensajeChatDto dto);
	List<MensajeChatDto> mensajesPorChat(String idChat);
}
