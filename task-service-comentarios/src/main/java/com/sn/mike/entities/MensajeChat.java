package com.sn.mike.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.sn.mike.dto.MensajeChatDto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name="mensaje_chat")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MensajeChat {
	
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(updatable = false)
	private String id;
    @ManyToOne
    @JoinColumn(name = "id_chat", referencedColumnName = "id")
	private Chat idChat;
    @ManyToOne
    @JoinColumn(name = "id_mensaje", referencedColumnName = "id")
	private Mensajes idMensaje;
    
	public MensajeChatDto to() {
		MensajeChatDto dto = new MensajeChatDto();
		dto.setIdChat(getIdChat().to());
		dto.setIdMensaje(getIdMensaje().to());
		return dto;
	}
    

}
