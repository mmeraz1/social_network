package com.sn.mike.entities;

import java.util.Date;
import java.util.List;
import java.io.Serializable;
import java.sql.Time;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.sn.mike.dto.MensajesDto;
import com.sn.mike.utils.Format;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Table(name="mensajes")
@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Mensajes implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(generator = "uuid2")
	@GenericGenerator(name = "uuid2", strategy = "uuid2")
	@Column(updatable = false)
	private String id;
	private String texto;
	private Date fecha;
	private Time hora;
	@ManyToOne
	@JoinColumn(name = "id_usuario", referencedColumnName = "id")
	private Usuarios idUsuario;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id")
    private List<MensajeChat> mensajesChatList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "id")
    private List<PublicacionMensaje> publicacionMensajeList;
	
	
	public MensajesDto to() {
		MensajesDto dto = new MensajesDto();
		Date date = getId() == null ? new Date() : getFecha();
		Time hour = getId() == null ? Format.hour() : getHora();
		if(getId() != null) {
			dto.setId(getId());
		}
		dto.setTexto(getTexto());
		dto.setFecha(date);
		dto.setHora(hour);
		return dto;
	}


	public Mensajes(String id) {
		this.id = id;
	}
	
	
	
	
}
