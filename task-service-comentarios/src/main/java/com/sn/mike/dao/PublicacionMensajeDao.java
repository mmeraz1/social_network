package com.sn.mike.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sn.mike.entities.MensajeChat;
import com.sn.mike.entities.PublicacionMensaje;

@Repository
public interface PublicacionMensajeDao extends JpaRepository<PublicacionMensaje, String>{

	List<PublicacionMensaje> findByPublicacionId(String idPublicacion);
}
