package com.sn.mike.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sn.mike.entities.MensajeChat;

@Repository
public interface MensajeChatDao extends JpaRepository<MensajeChat, String>{

	List<MensajeChat> findByIdChatId(String idChat);
}
