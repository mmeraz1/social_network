package com.sn.mike.dto;

import java.sql.Time;
import java.util.Date;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.sn.mike.entities.Chat;
import com.sn.mike.entities.Mensajes;
import com.sn.mike.entities.Publicaciones;
import com.sn.mike.entities.Usuarios;
import com.sn.mike.utils.Format;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@JsonInclude(Include.NON_NULL)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UsuariosDto {

	private String id;
	private String nombre;
	private String apellido;
	private Date fecha;
	private Time hora;
	private String correo;
    private List<Mensajes> mensajesList;
    private List<Publicaciones> publicacionesList;
    private List<Chat> chatsList;
    
    public UsuariosDto from(Usuarios entity) {
    	UsuariosDto dto = new UsuariosDto();
    	dto.setNombre(entity.getNombre());
    	dto.setApellido(entity.getApellido());
    	dto.setCorreo(entity.getCorreo());
    	dto.setFecha(entity.getFecha());
    	dto.setHora(entity.getHora());
    	return dto;
    }
    
    public Usuarios to() {
    	Usuarios entity = new Usuarios();
    	entity.setId(getId());
    	entity.setNombre(getNombre());
    	entity.setApellido(getApellido());
    	entity.setCorreo(getCorreo());
    	entity.setFecha(new Date());
    	entity.setHora(Format.hour());
    	return entity;
    }
    
    
    public Usuarios toId() {
    	Usuarios entity = new Usuarios();
    	entity.setId(getId());
    	return entity;
    }
    
    
    
}